{
  asciidoctor = {
    groups = ["default"];
    platforms = [];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0k3lijm4dmiz977bfmpclk5glj5jwv7bidamwwwywm60ywb0n4n4";
      type = "gem";
    };
    version = "2.0.15";
  };
  asciidoctor-latex = {
    dependencies = ["asciidoctor" "asciimath" "htmlentities"];
    groups = ["default"];
    platforms = [];
    source = {
      fetchSubmodules = false;
      rev = "c199ee9641cfb75658d27c3a8e0636f32a20b0a9";
      sha256 = "0i7r7f1sazqya4z3dlycjngzw5wz3fqch7mgzrkz4mdy7r299gbw";
      type = "git";
      url = "https://gitlab.com/seamsay/asciidoctor-latex.git";
    };
    version = "0.1.0.dev";
  };
  asciidoctor-latex-bibtex = {
    dependencies = ["asciidoctor"];
    groups = ["default"];
    platforms = [];
    source = {
      fetchSubmodules = false;
      rev = "824b9b7ab35546c815a8921b97f2ff6c22394d5c";
      sha256 = "1h10bwg68zfgb0kjgk0vmz6qm6089vs64bz6kw5z7fv1phz2sdbp";
      type = "git";
      url = "https://gitlab.com/seamsay/asciidoctor-latex-bibtex.git";
    };
    version = "0.1.0.dev";
  };
  asciimath = {
    groups = ["default"];
    platforms = [];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0h4fz93pf96y5syxwpv0vibjf7lidv2718ikpvyd2vy8c1am8zyn";
      type = "gem";
    };
    version = "2.0.3";
  };
  htmlentities = {
    groups = ["default"];
    platforms = [];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1nkklqsn8ir8wizzlakncfv42i32wc0w9hxp00hvdlgjr7376nhj";
      type = "gem";
    };
    version = "4.3.4";
  };
}
