module Mook

using Cuba
using LinearAlgebra
using StaticArrays

# Reproduction of figure 13a in Interaction-Stabilized Topological Magnon Insulator in Ferromagnets.
# NOTE: I swap \vec{p} and \vec{q}.
# NOTE: This paper used a lattice that was rotated 90 degree compared to ours.

@enum Band up = 1 down = -1

const NEAREST_NEIGHBOURS = SA[SA[1.0, 0.0], SA[-1/2, sqrt(3)/2], SA[-1/2, -sqrt(3)/2]]
const UNIT_CELL_SITES = 2

γ(k::SVector{2,Float64}) =
    let τ1 = NEAREST_NEIGHBOURS[1], τ2 = NEAREST_NEIGHBOURS[2], τ3 = NEAREST_NEIGHBOURS[3]
        θ1 = k ⋅ τ1
        θ2 = k ⋅ τ2
        θ3 = k ⋅ τ3

        cos(θ1) + cos(θ2) + cos(θ3) + im * (sin(θ1) + sin(θ2) + sin(θ3))
    end

E_J(k::SVector{2,Float64}, band::Band, S::Float64, B_J::Float64) =
    let b = Int(band)
        S * (3 + b * abs(γ(k))) + B_J
    end

ED_J(S, B_J) = 3S + B_J

ρ(ω_J, kT_J) = (exp(ω_J / kT_J) - 1)^-1

const δ = 1e-4

G(k::SVector{2,Float64}, ω::Float64, T::Float64, D_J::Float64, S::Float64, B_J::Float64) =
    (
        ω .+ δ * im .- SA[E_J(k, down, S, B_J) 0.0; 0.0 E_J(k, up, S, B_J)] -
        Σ_1(k, ω, T, D_J, S, B_J)[1]
    )^-1

S(k::SVector{2,Float64}, ω::Float64, T::Float64, D_J::Float64, S::Float64, B_J::Float64) =
    -1 * (1 - ρ(ω, T)) * imag(sum(diag(G(k, ω, T, D_J, S, B_J)))) / π

function Σ_1(k, ω, T, D_J, S, B_J)
    result = MMatrix{2,2,Complex{Float64}}(undef)
    cubas = Matrix(undef, 2, 2)

    # Down-Down
    let integral = cuhre(
            function (x, output)
                (a1, a2) = x

                left = -π / 3
                right = π / 3
                bottom = -sqrt(3) * π / 3
                top = sqrt(3) * π / 3

                p1 = left + a1 * (right - left)
                p2 = bottom + a2 * (top - bottom)

                p = SA[p1, p2]

                E_k_u = E_J(k, up, S, B_J)
                E_k_d = E_J(k, down, S, B_J)
                E_p_u = E_J(p, up, S, B_J)
                E_p_d = E_J(p, down, S, B_J)
                E_kmp_u = E_J(k - p, up, S, B_J)
                E_kmp_d = E_J(k - p, down, S, B_J)
                E_kpp_u = E_J(k + p, up, S, B_J)
                E_kpp_d = E_J(k + p, down, S, B_J)

                ρ_k_u = ρ(E_k_u, T)
                ρ_k_d = ρ(E_k_d, T)
                ρ_p_u = ρ(E_p_u, T)
                ρ_p_d = ρ(E_p_d, T)
                ρ_kmp_u = ρ(E_kmp_u, T)
                ρ_kmp_d = ρ(E_kmp_d, T)
                ρ_kpp_u = ρ(E_kpp_u, T)
                ρ_kpp_d = ρ(E_kpp_d, T)

                #! format: off
                integrand = 1/2 * V_ddd(k, p, k - p, D_J, S) * conj(V_ddd(p, k - p, k, D_J, S)) * (ρ_p_d + ρ_kmp_d + 1) / (ω + δ*im - E_p_d - E_kmp_d)
                          + 1/2 * V_ddu(k, p, k - p, D_J, S) * conj(V_dud(p, k - p, k, D_J, S)) * (ρ_p_d + ρ_kmp_u + 1) / (ω + δ*im - E_p_d - E_kmp_u)
                          + 1/2 * V_dud(k, p, k - p, D_J, S) * conj(V_udd(p, k - p, k, D_J, S)) * (ρ_p_u + ρ_kmp_d + 1) / (ω + δ*im - E_p_u - E_kmp_d)
                          + 1/2 * V_duu(k, p, k - p, D_J, S) * conj(V_uud(p, k - p, k, D_J, S)) * (ρ_p_u + ρ_kmp_u + 1) / (ω + δ*im - E_p_u - E_kmp_u)
                          + V_ddd(k + p, k, p, D_J, S) * conj(V_ddd(k, p, k + p, D_J, S)) * (ρ_p_d - ρ_kpp_d) / (ω + δ*im + E_p_d - E_kpp_d)
                          + V_udd(k + p, k, p, D_J, S) * conj(V_ddu(k, p, k + p, D_J, S)) * (ρ_p_d - ρ_kpp_u) / (ω + δ*im + E_p_d - E_kpp_u)
                          + V_ddu(k + p, k, p, D_J, S) * conj(V_dud(k, p, k + p, D_J, S)) * (ρ_p_u - ρ_kpp_d) / (ω + δ*im + E_p_u - E_kpp_d)
                          + V_udu(k + p, k, p, D_J, S) * conj(V_duu(k, p, k + p, D_J, S)) * (ρ_p_u - ρ_kpp_u) / (ω + δ*im + E_p_u - E_kpp_u)
                #! format: on
                integrand /= UNIT_CELL_SITES

                output[1] = real(integrand)
                output[2] = imag(integrand)
            end,
            2,
            2,
        )
        result[1, 1] = integral.integral[1] + integral.integral[2] * im
        cubas[1, 1] = integral
    end

    # Down-Up
    let integral = cuhre(
            function (x, output)
                (a1, a2) = x

                left = -π / 3
                right = π / 3
                bottom = -sqrt(3) * π / 3
                top = sqrt(3) * π / 3

                p1 = left + a1 * (right - left)
                p2 = bottom + a2 * (top - bottom)

                p = SA[p1, p2]

                E_k_u = E_J(k, up, S, B_J)
                E_k_d = E_J(k, down, S, B_J)
                E_p_u = E_J(p, up, S, B_J)
                E_p_d = E_J(p, down, S, B_J)
                E_kmp_u = E_J(k - p, up, S, B_J)
                E_kmp_d = E_J(k - p, down, S, B_J)
                E_kpp_u = E_J(k + p, up, S, B_J)
                E_kpp_d = E_J(k + p, down, S, B_J)

                ρ_k_u = ρ(E_k_u, T)
                ρ_k_d = ρ(E_k_d, T)
                ρ_p_u = ρ(E_p_u, T)
                ρ_p_d = ρ(E_p_d, T)
                ρ_kmp_u = ρ(E_kmp_u, T)
                ρ_kmp_d = ρ(E_kmp_d, T)
                ρ_kpp_u = ρ(E_kpp_u, T)
                ρ_kpp_d = ρ(E_kpp_d, T)

                #! format: off
                integrand = 1/2 * V_ddd(k, p, k - p, D_J, S) * conj(V_ddu(p, k - p, k, D_J, S)) * (ρ_p_d + ρ_kmp_d + 1) / (ω + δ*im - E_p_d - E_kmp_d)
                          + 1/2 * V_ddu(k, p, k - p, D_J, S) * conj(V_duu(p, k - p, k, D_J, S)) * (ρ_p_d + ρ_kmp_u + 1) / (ω + δ*im - E_p_d - E_kmp_u)
                          + 1/2 * V_dud(k, p, k - p, D_J, S) * conj(V_udu(p, k - p, k, D_J, S)) * (ρ_p_u + ρ_kmp_d + 1) / (ω + δ*im - E_p_u - E_kmp_d)
                          + 1/2 * V_duu(k, p, k - p, D_J, S) * conj(V_uuu(p, k - p, k, D_J, S)) * (ρ_p_u + ρ_kmp_u + 1) / (ω + δ*im - E_p_u - E_kmp_u)
                          + V_dud(k + p, k, p, D_J, S) * conj(V_ddd(k, p, k + p, D_J, S)) * (ρ_p_d - ρ_kpp_d) / (ω + δ*im + E_p_d - E_kpp_d)
                          + V_uud(k + p, k, p, D_J, S) * conj(V_ddu(k, p, k + p, D_J, S)) * (ρ_p_d - ρ_kpp_u) / (ω + δ*im + E_p_d - E_kpp_u)
                          + V_duu(k + p, k, p, D_J, S) * conj(V_dud(k, p, k + p, D_J, S)) * (ρ_p_u - ρ_kpp_d) / (ω + δ*im + E_p_u - E_kpp_d)
                          + V_uuu(k + p, k, p, D_J, S) * conj(V_duu(k, p, k + p, D_J, S)) * (ρ_p_u - ρ_kpp_u) / (ω + δ*im + E_p_u - E_kpp_u)
                #! format: on
                integrand /= UNIT_CELL_SITES

                output[1] = real(integrand)
                output[2] = imag(integrand)
            end,
            2,
            2,
        )
        result[1, 2] = integral.integral[1] + integral.integral[2] * im
        cubas[1, 2] = integral
    end

    # Up-Down
    let integral = cuhre(
            function (x, output)
                (a1, a2) = x

                left = -π / 3
                right = π / 3
                bottom = -sqrt(3) * π / 3
                top = sqrt(3) * π / 3

                p1 = left + a1 * (right - left)
                p2 = bottom + a2 * (top - bottom)

                p = SA[p1, p2]

                E_k_u = E_J(k, up, S, B_J)
                E_k_d = E_J(k, down, S, B_J)
                E_p_u = E_J(p, up, S, B_J)
                E_p_d = E_J(p, down, S, B_J)
                E_kmp_u = E_J(k - p, up, S, B_J)
                E_kmp_d = E_J(k - p, down, S, B_J)
                E_kpp_u = E_J(k + p, up, S, B_J)
                E_kpp_d = E_J(k + p, down, S, B_J)

                ρ_k_u = ρ(E_k_u, T)
                ρ_k_d = ρ(E_k_d, T)
                ρ_p_u = ρ(E_p_u, T)
                ρ_p_d = ρ(E_p_d, T)
                ρ_kmp_u = ρ(E_kmp_u, T)
                ρ_kmp_d = ρ(E_kmp_d, T)
                ρ_kpp_u = ρ(E_kpp_u, T)
                ρ_kpp_d = ρ(E_kpp_d, T)

                #! format: off
                integrand = 1/2 * V_udd(k, p, k - p, D_J, S) * conj(V_ddd(p, k - p, k, D_J, S)) * (ρ_p_d + ρ_kmp_d + 1) / (ω + δ*im - E_p_d - E_kmp_d)
                          + 1/2 * V_udu(k, p, k - p, D_J, S) * conj(V_dud(p, k - p, k, D_J, S)) * (ρ_p_d + ρ_kmp_u + 1) / (ω + δ*im - E_p_d - E_kmp_u)
                          + 1/2 * V_uud(k, p, k - p, D_J, S) * conj(V_udd(p, k - p, k, D_J, S)) * (ρ_p_u + ρ_kmp_d + 1) / (ω + δ*im - E_p_u - E_kmp_d)
                          + 1/2 * V_uuu(k, p, k - p, D_J, S) * conj(V_uud(p, k - p, k, D_J, S)) * (ρ_p_u + ρ_kmp_u + 1) / (ω + δ*im - E_p_u - E_kmp_u)
                          + V_ddd(k + p, k, p, D_J, S) * conj(V_udd(k, p, k + p, D_J, S)) * (ρ_p_d - ρ_kpp_d) / (ω + δ*im + E_p_d - E_kpp_d)
                          + V_udd(k + p, k, p, D_J, S) * conj(V_udu(k, p, k + p, D_J, S)) * (ρ_p_d - ρ_kpp_u) / (ω + δ*im + E_p_d - E_kpp_u)
                          + V_ddu(k + p, k, p, D_J, S) * conj(V_uud(k, p, k + p, D_J, S)) * (ρ_p_u - ρ_kpp_d) / (ω + δ*im + E_p_u - E_kpp_d)
                          + V_udu(k + p, k, p, D_J, S) * conj(V_uuu(k, p, k + p, D_J, S)) * (ρ_p_u - ρ_kpp_u) / (ω + δ*im + E_p_u - E_kpp_u)
                #! format: on
                integrand /= UNIT_CELL_SITES

                output[1] = real(integrand)
                output[2] = imag(integrand)
            end,
            2,
            2,
        )
        result[2, 1] = integral.integral[1] + integral.integral[2] * im
        cubas[2, 1] = integral
    end

    # Up-Up
    let integral = cuhre(
            function (x, output)
                (a1, a2) = x

                left = -π / 3
                right = π / 3
                bottom = -sqrt(3) * π / 3
                top = sqrt(3) * π / 3

                p1 = left + a1 * (right - left)
                p2 = bottom + a2 * (top - bottom)

                p = SA[p1, p2]

                E_k_u = E_J(k, up, S, B_J)
                E_k_d = E_J(k, down, S, B_J)
                E_p_u = E_J(p, up, S, B_J)
                E_p_d = E_J(p, down, S, B_J)
                E_kmp_u = E_J(k - p, up, S, B_J)
                E_kmp_d = E_J(k - p, down, S, B_J)
                E_kpp_u = E_J(k + p, up, S, B_J)
                E_kpp_d = E_J(k + p, down, S, B_J)

                ρ_k_u = ρ(E_k_u, T)
                ρ_k_d = ρ(E_k_d, T)
                ρ_p_u = ρ(E_p_u, T)
                ρ_p_d = ρ(E_p_d, T)
                ρ_kmp_u = ρ(E_kmp_u, T)
                ρ_kmp_d = ρ(E_kmp_d, T)
                ρ_kpp_u = ρ(E_kpp_u, T)
                ρ_kpp_d = ρ(E_kpp_d, T)

                #! format: off
                integrand = 1/2 * V_udd(k, p, k - p, D_J, S) * conj(V_ddd(p, k - p, k, D_J, S)) * (ρ_p_d + ρ_kmp_d + 1) / (ω + δ*im - E_p_d - E_kmp_d)
                          + 1/2 * V_udu(k, p, k - p, D_J, S) * conj(V_dud(p, k - p, k, D_J, S)) * (ρ_p_d + ρ_kmp_u + 1) / (ω + δ*im - E_p_d - E_kmp_u)
                          + 1/2 * V_uud(k, p, k - p, D_J, S) * conj(V_udd(p, k - p, k, D_J, S)) * (ρ_p_u + ρ_kmp_d + 1) / (ω + δ*im - E_p_u - E_kmp_d)
                          + 1/2 * V_uuu(k, p, k - p, D_J, S) * conj(V_uud(p, k - p, k, D_J, S)) * (ρ_p_u + ρ_kmp_u + 1) / (ω + δ*im - E_p_u - E_kmp_u)
                          + V_dud(k + p, k, p, D_J, S) * conj(V_udd(k, p, k + p, D_J, S)) * (ρ_p_d - ρ_kpp_d) / (ω + δ*im + E_p_d - E_kpp_d)
                          + V_uud(k + p, k, p, D_J, S) * conj(V_udu(k, p, k + p, D_J, S)) * (ρ_p_d - ρ_kpp_u) / (ω + δ*im + E_p_d - E_kpp_u)
                          + V_duu(k + p, k, p, D_J, S) * conj(V_uud(k, p, k + p, D_J, S)) * (ρ_p_u - ρ_kpp_d) / (ω + δ*im + E_p_u - E_kpp_d)
                          + V_uuu(k + p, k, p, D_J, S) * conj(V_uuu(k, p, k + p, D_J, S)) * (ρ_p_u - ρ_kpp_u) / (ω + δ*im + E_p_u - E_kpp_u)
                #! format: on
                integrand /= UNIT_CELL_SITES

                output[1] = real(integrand)
                output[2] = imag(integrand)
            end,
            2,
            2,
        )
        result[2, 2] = integral.integral[1] + integral.integral[2] * im
        cubas[2, 2] = integral
    end

    result, cubas
end

const φ1 = angle(1 - 0im)
const φ2 = angle(-1 / 2 - (-sqrt(3) / 2) * im)
const φ3 = angle(-1 / 2 - (sqrt(3) / 2) * im)

# TODO: Make these tensors.
HP_V_121(_::SVector{2,Float64}, p::SVector{2,Float64}, D::Float64, S::Float64) =
    let τ1 = NEAREST_NEIGHBOURS[1], τ2 = NEAREST_NEIGHBOURS[2], τ3 = NEAREST_NEIGHBOURS[3]
        -D *
        sqrt(S / 2) *
        (exp(im * (φ1 - p ⋅ τ1)) + exp(im * (φ2 - p ⋅ τ2)) + exp(im * (φ3 - p ⋅ τ3)))
    end

HP_V_211(k::SVector{2,Float64}, _::SVector{2,Float64}, D::Float64, S::Float64) =
    let τ1 = NEAREST_NEIGHBOURS[1], τ2 = NEAREST_NEIGHBOURS[2], τ3 = NEAREST_NEIGHBOURS[3]
        -D *
        sqrt(S / 2) *
        (exp(im * (φ1 - k ⋅ τ1)) + exp(im * (φ2 - k ⋅ τ2)) + exp(im * (φ3 - k ⋅ τ3)))
    end

HP_V_212(_::SVector{2,Float64}, p::SVector{2,Float64}, D::Float64, S::Float64) =
    let τ1 = NEAREST_NEIGHBOURS[1], τ2 = NEAREST_NEIGHBOURS[2], τ3 = NEAREST_NEIGHBOURS[3]
        D *
        sqrt(S / 2) *
        (exp(im * (φ1 + p ⋅ τ1)) + exp(im * (φ2 + p ⋅ τ2)) + exp(im * (φ3 + p ⋅ τ3)))
    end

HP_V_122(k::SVector{2,Float64}, _::SVector{2,Float64}, D::Float64, S::Float64) =
    let τ1 = NEAREST_NEIGHBOURS[1], τ2 = NEAREST_NEIGHBOURS[2], τ3 = NEAREST_NEIGHBOURS[3]
        D *
        sqrt(S / 2) *
        (exp(im * (φ1 + k ⋅ τ1)) + exp(im * (φ2 + k ⋅ τ2)) + exp(im * (φ3 + k ⋅ τ3)))
    end

U_1u(k::SVector{2,Float64}) = 1 / sqrt(2) * exp(im * angle(γ(k)) / 2)
U_1d(k::SVector{2,Float64}) = 1 / sqrt(2) * exp(im * angle(γ(k)) / 2)
U_2u(k::SVector{2,Float64}) = 1 / sqrt(2) * exp(-im * angle(γ(k)) / 2)
U_2d(k::SVector{2,Float64}) = 1 / sqrt(2) * -exp(-im * angle(γ(k)) / 2)

V_uuu(
    k::SVector{2,Float64},
    p::SVector{2,Float64},
    q::SVector{2,Float64},
    D::Float64,
    S::Float64,
) =
    HP_V_121(k, p, D, S) * conj(U_1u(k)) * conj(U_2u(p)) * U_1u(q) +
    HP_V_122(k, p, D, S) * conj(U_1u(k)) * conj(U_2u(p)) * U_2u(q) +
    HP_V_211(k, p, D, S) * conj(U_2u(k)) * conj(U_1u(p)) * U_1u(q) +
    HP_V_212(k, p, D, S) * conj(U_2u(k)) * conj(U_1u(p)) * U_2u(q)

V_uud(
    k::SVector{2,Float64},
    p::SVector{2,Float64},
    q::SVector{2,Float64},
    D::Float64,
    S::Float64,
) =
    HP_V_121(k, p, D, S) * conj(U_1u(k)) * conj(U_2u(p)) * U_1d(q) +
    HP_V_122(k, p, D, S) * conj(U_1u(k)) * conj(U_2u(p)) * U_2d(q) +
    HP_V_211(k, p, D, S) * conj(U_2u(k)) * conj(U_1u(p)) * U_1d(q) +
    HP_V_212(k, p, D, S) * conj(U_2u(k)) * conj(U_1u(p)) * U_2d(q)

V_udu(
    k::SVector{2,Float64},
    p::SVector{2,Float64},
    q::SVector{2,Float64},
    D::Float64,
    S::Float64,
) =
    HP_V_121(k, p, D, S) * conj(U_1u(k)) * conj(U_2d(p)) * U_1u(q) +
    HP_V_122(k, p, D, S) * conj(U_1u(k)) * conj(U_2d(p)) * U_2u(q) +
    HP_V_211(k, p, D, S) * conj(U_2u(k)) * conj(U_1d(p)) * U_1u(q) +
    HP_V_212(k, p, D, S) * conj(U_2u(k)) * conj(U_1d(p)) * U_2u(q)

V_udd(
    k::SVector{2,Float64},
    p::SVector{2,Float64},
    q::SVector{2,Float64},
    D::Float64,
    S::Float64,
) =
    HP_V_121(k, p, D, S) * conj(U_1u(k)) * conj(U_2d(p)) * U_1d(q) +
    HP_V_122(k, p, D, S) * conj(U_1u(k)) * conj(U_2d(p)) * U_2d(q) +
    HP_V_211(k, p, D, S) * conj(U_2u(k)) * conj(U_1d(p)) * U_1d(q) +
    HP_V_212(k, p, D, S) * conj(U_2u(k)) * conj(U_1d(p)) * U_2d(q)

V_duu(
    k::SVector{2,Float64},
    p::SVector{2,Float64},
    q::SVector{2,Float64},
    D::Float64,
    S::Float64,
) =
    HP_V_121(k, p, D, S) * conj(U_1d(k)) * conj(U_2u(p)) * U_1u(q) +
    HP_V_122(k, p, D, S) * conj(U_1d(k)) * conj(U_2u(p)) * U_2u(q) +
    HP_V_211(k, p, D, S) * conj(U_2d(k)) * conj(U_1u(p)) * U_1u(q) +
    HP_V_212(k, p, D, S) * conj(U_2d(k)) * conj(U_1u(p)) * U_2u(q)

V_dud(
    k::SVector{2,Float64},
    p::SVector{2,Float64},
    q::SVector{2,Float64},
    D::Float64,
    S::Float64,
) =
    HP_V_121(k, p, D, S) * conj(U_1d(k)) * conj(U_2u(p)) * U_1d(q) +
    HP_V_122(k, p, D, S) * conj(U_1d(k)) * conj(U_2u(p)) * U_2d(q) +
    HP_V_211(k, p, D, S) * conj(U_2d(k)) * conj(U_1u(p)) * U_1d(q) +
    HP_V_212(k, p, D, S) * conj(U_2d(k)) * conj(U_1u(p)) * U_2d(q)

V_ddu(
    k::SVector{2,Float64},
    p::SVector{2,Float64},
    q::SVector{2,Float64},
    D::Float64,
    S::Float64,
) =
    HP_V_121(k, p, D, S) * conj(U_1d(k)) * conj(U_2d(p)) * U_1u(q) +
    HP_V_122(k, p, D, S) * conj(U_1d(k)) * conj(U_2d(p)) * U_2u(q) +
    HP_V_211(k, p, D, S) * conj(U_2d(k)) * conj(U_1d(p)) * U_1u(q) +
    HP_V_212(k, p, D, S) * conj(U_2d(k)) * conj(U_1d(p)) * U_2u(q)

V_ddd(
    k::SVector{2,Float64},
    p::SVector{2,Float64},
    q::SVector{2,Float64},
    D::Float64,
    S::Float64,
) =
    HP_V_121(k, p, D, S) * conj(U_1d(k)) * conj(U_2d(p)) * U_1d(q) +
    HP_V_122(k, p, D, S) * conj(U_1d(k)) * conj(U_2d(p)) * U_2d(q) +
    HP_V_211(k, p, D, S) * conj(U_2d(k)) * conj(U_1d(p)) * U_1d(q) +
    HP_V_212(k, p, D, S) * conj(U_2d(k)) * conj(U_1d(p)) * U_2d(q)

end  # module
