V_1111(
    k::SVector{3,Float64},
    p::SVector{3,Float64},
    q::SVector{3,Float64},
    parameters::Parameters,
) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        ξ_k = ξ(k),
        ξ_p = ξ(p),
        ξ_q = ξ(q),
        ξ_kpq = ξ(k + p - q)

        #! format: off
        - (D / 2 + 3 * D_BQ * S^2 + 3 * K * S^2 / 2)                * exp( (ξ_k + ξ_p - ξ_q - ξ_kpq) * im / 2) +
        - (J / 8 + 5 * K * S^2 / 4)                  * γ(k)         * exp( (ξ_p - ξ_k - ξ_q - ξ_kpq) * im / 2) +
        - K * S^2 / 2                                * γ(k + p)     * exp(-(ξ_k + ξ_p + ξ_q + ξ_kpq) * im / 2) +
        - (J / 8 + 5 * K * S^2 / 4)                  * γ(-q)        * exp( (ξ_k + ξ_p + ξ_q - ξ_kpq) * im / 2) +
        - (J / 2 + λ / 2 + 3 * K * S^2)              * γ(k - q)     * exp( (ξ_p + ξ_q - ξ_k - ξ_kpq) * im / 2) +
        - (J / 8 + 5 * K * S^2 / 4)                  * γ(k + p - q) * exp( (ξ_q - ξ_k - ξ_p - ξ_kpq) * im / 2) +
        - K * S^2 / 2                                * γ(-k - p)    * exp( (ξ_k + ξ_p + ξ_q + ξ_kpq) * im / 2) +
        - (J / 8 + 5 * K * S^2)                      * γ(-p)        * exp( (ξ_p + ξ_q + ξ_kpq - ξ_k) * im / 2) +
        - (D / 2 + 3 * D_BQ * S^2 + 3 * K * S^2 / 2)                * exp( (ξ_q + ξ_kpq - ξ_k - ξ_p) * im / 2)
        #! format: on
    end

V_1112(
    k::SVector{3,Float64},
    p::SVector{3,Float64},
    q::SVector{3,Float64},
    parameters::Parameters,
) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        ξ_k = ξ(k),
        ξ_p = ξ(p),
        ξ_q = ξ(q),
        ξ_kpq = ξ(k + p - q)

        #! format: off
        - (D + 6 * D_BQ * S^2 + 3 * K * S^2)                * exp((ξ_k + ξ_p - ξ_q - ξ_kpq) * im / 2)                                                                                  +
          (J / 8 + 5 * K * S^2 / 4)                         * exp(-(ξ_q + ξ_kpq) * im / 2)             * (γ(k) * exp((ξ_p - ξ_k) * im / 2) - γ(p) * exp((ξ_k - ξ_p) * im / 2))         +
          K * S^2                            * γ(k + p)     * exp(-(ξ_k + ξ_p + ξ_q + ξ_kpq) * im / 2)                                                                                 +
        - (J / 4 + 5 * K * S^2 / 2)          * γ(-q)        * exp((ξ_k + ξ_p + ξ_q - ξ_kpq) * im / 2)                                                                                  +
          (J / 2 + λ / 2 + 3 * K * S^2)                     * exp((ξ_q - ξ_kpq) * im / 2)              * (γ(k - q) * exp((ξ_p - ξ_k) * im / 2) - γ(p - q) * exp((ξ_k - ξ_p) * im / 2)) +
          (J / 4 + 5 * K * S^2 / 2)          * γ(k + p - q) * exp((ξ_q - ξ_k - ξ_p - ξ_kpq) * im / 2)                                                                                  +
        - K * S^2                            * γ(-k - p)    * exp((ξ_k + ξ_p + ξ_q + ξ_kpq) * im / 2)                                                                                  +
          (J / 8 + 5 * K * S^2 / 4)                         * exp((ξ_q + ξ_kpq) * im / 2)              * (γ(-p) * exp((ξ_p - ξ_k) * im / 2) - γ(-k) * exp((ξ_k - ξ_p) * im / 2))       +
          (D + 6 * D_BQ * S^2 + 3 * K * S^2)                * exp((ξ_q + ξ_kpq - ξ_k - ξ_p) * im / 2)
        #! format: on
    end

V_1122(
    k::SVector{3,Float64},
    p::SVector{3,Float64},
    q::SVector{3,Float64},
    parameters::Parameters,
) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        ξ_k = ξ(k),
        ξ_p = ξ(p),
        ξ_q = ξ(q),
        ξ_kpq = ξ(k + p - q)

        #! format: off
        -((1 / 2) * D + (3) * D_BQ * S^2 + (3 / 2) * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p - ξ_q - ξ_kpq)) -
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(k) * exp(im * (1 / 2) * (ξ_p - ξ_k - ξ_q - ξ_kpq)) -
        (1 / 2) * K * S^2 * γ(k + p) * exp(-im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) -
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(-q) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q - ξ_kpq)) +
        ((1 / 2) * J + (1 / 2) * λ + (3) * K * S^2) * γ(k - q) * exp(im * (1 / 2) * (ξ_p + ξ_q - ξ_k - ξ_kpq)) -
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(k + p - q) * exp(im * (1 / 2) * (ξ_q - ξ_k - ξ_p - ξ_kpq)) -
        (1 / 2) * K * S^2 * γ(-k - p) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(-p) * exp(im * (1 / 2) * (ξ_p + ξ_q + ξ_kpq - ξ_k)) -
        ((1 / 2) * D + (3) * D_BQ * S^2 + (3 / 2) * K * S^2) * exp(im * (1 / 2) * (ξ_q + ξ_kpq - ξ_k - ξ_p))
        #! format: on
    end

V_1211(
    k::SVector{3,Float64},
    p::SVector{3,Float64},
    q::SVector{3,Float64},
    parameters::Parameters,
) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        ξ_k = ξ(k),
        ξ_p = ξ(p),
        ξ_q = ξ(q),
        ξ_kpq = ξ(k + p - q)

        #! format: off
        -(D + 6 * D_BQ * S^2 + (3) * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p - ξ_q - ξ_kpq)) -
        ((1 / 4) * J + (5 / 2) * K * S^2) * γ(k) * exp(im * (1 / 2) * (ξ_p - ξ_k - ξ_q - ξ_kpq)) -
        K * S^2 * γ(k + p) * exp(-im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p)) * ( γ(-q) * exp(im * (1 / 2) * (ξ_q - ξ_kpq)) - γ(q - k - p) * exp(im * (1 / 2) * (ξ_kpq - ξ_q)) ) +
        ((1 / 2) * J + (1 / 2) * λ + (3) * K * S^2) * exp(im * (1 / 2) * (ξ_p - ξ_k)) * ( γ(k - q) * exp(im * (1 / 2) * (ξ_q - ξ_kpq)) - γ(q - p) * exp(im * (1 / 2) * (ξ_kpq - ξ_q)) ) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * exp(-im * (1 / 2) * (ξ_k + ξ_p)) * ( γ(k + p - q) * exp(im * (1 / 2) * (ξ_q - ξ_kpq)) - γ(q) * exp(im * (1 / 2) * (ξ_kpq - ξ_q)) ) +
        K * S^2 * γ(-k - p) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 4) * J + (5 / 2) * K * S^2) * γ(-p) * exp(im * (1 / 2) * (ξ_p + ξ_q + ξ_kpq - ξ_k)) +
        (D + 6 * D_BQ * S^2 + (3) * K * S^2) * exp(im * (1 / 2) * (ξ_q + ξ_kpq - ξ_k - ξ_p))
        #! format: on
    end

V_1212(
    k::SVector{3,Float64},
    p::SVector{3,Float64},
    q::SVector{3,Float64},
    parameters::Parameters,
) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        ξ_k = ξ(k),
        ξ_p = ξ(p),
        ξ_q = ξ(q),
        ξ_kpq = ξ(k + p - q)

        #! format: off
        -(2 * D + 12 * D_BQ * S^2 + 6 * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p - ξ_q - ξ_kpq)) +
        ((1 / 4) * J + (5 / 2) * K * S^2) * exp(-im * (1 / 2) * (ξ_q + ξ_kpq)) * (γ(k) * exp(im * (1 / 2) * (ξ_p - ξ_k)) - γ(p) * exp(im * (1 / 2) * (ξ_k - ξ_p))) +
        2 * K * S^2 * γ(k + p) * exp(-im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 4) * J + (5 / 2) * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p)) * ( γ(-q) * exp(im * (1 / 2) * (ξ_q - ξ_kpq)) - γ(q - k - p) * exp(im * (1 / 2) * (ξ_kpq - ξ_q)) ) +
        ((1 / 2) * J + (1 / 2) * λ + (3) * K * S^2) * ( γ(q - p) * exp(im * (1 / 2) * (ξ_p + ξ_q - ξ_k - ξ_kpq)) + γ(p - q) * exp(im * (1 / 2) * (ξ_k + ξ_q - ξ_p - ξ_kpq)) - γ(k - q) * exp(im * (1 / 2) * (ξ_p + ξ_q - ξ_k - ξ_kpq)) - γ(q - k) * exp(im * (1 / 2) * (ξ_k + ξ_kpq - ξ_p - ξ_q)) ) -
        ((1 / 4) * J + (5 / 2) * K * S^2) * exp(-im * (1 / 2) * (ξ_k + ξ_p)) * ( γ(k + p - q) * exp(im * (1 / 2) * (ξ_q - ξ_kpq)) - γ(q) * exp(im * (1 / 2) * (ξ_kpq - ξ_q)) ) +
        2 * K * S^2 * γ(-k - p) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) -
        ((1 / 4) * J + (5 / 2) * K * S^2) * exp(im * (1 / 2) * (ξ_q + ξ_kpq)) * (γ(-p) * exp(im * (1 / 2) * (ξ_p - ξ_k)) - γ(-k) * exp(im * (1 / 2) * (ξ_k - ξ_p))) -
        (2 * D + 12 * D_BQ * S^2 + 6 * K * S^2) * exp(im * (1 / 2) * (ξ_q + ξ_kpq - ξ_k - ξ_p))
        #! format: on
    end

V_1222(
    k::SVector{3,Float64},
    p::SVector{3,Float64},
    q::SVector{3,Float64},
    parameters::Parameters,
) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        ξ_k = ξ(k),
        ξ_p = ξ(p),
        ξ_q = ξ(q),
        ξ_kpq = ξ(k + p - q)

        #! format: off
        -(D + 6 * D_BQ * S^2 + (3) * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p - ξ_q - ξ_kpq)) -
        ((1 / 4) * J + (5 / 2) * K * S^2) * γ(k) * exp(im * (1 / 2) * (ξ_p - ξ_k - ξ_q - ξ_kpq)) -
        K * S^2 * γ(k + p) * exp(-im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p)) * ( γ(-q) * exp(im * (1 / 2) * (ξ_q - ξ_kpq)) - γ(q - k - p) * exp(im * (1 / 2) * (ξ_kpq - ξ_q)) ) +
        ((1 / 2) * J + (1 / 2) * λ + (3) * K * S^2) * exp(im * (1 / 2) * (ξ_p - ξ_k)) * ( γ(q - p) * exp(im * (1 / 2) * (ξ_kpq - ξ_q)) - γ(k - q) * exp(im * (1 / 2) * (ξ_q - ξ_kpq)) ) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * exp(-im * (1 / 2) * (ξ_k + ξ_p)) * ( γ(k + p - q) * exp(im * (1 / 2) * (ξ_q - ξ_kpq)) - γ(q) * exp(im * (1 / 2) * (ξ_kpq - ξ_q)) ) +
        K * S^2 * γ(-k - p) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) -
        ((1 / 4) * J + (5 / 2) * K * S^2) * γ(-p) * exp(im * (1 / 2) * (ξ_p + ξ_q + ξ_kpq - ξ_k)) +
        (D + 6 * D_BQ * S^2 + (3) * K * S^2) * exp(im * (1 / 2) * (ξ_q + ξ_kpq - ξ_k - ξ_p))
        #! format: on
    end

V_2211(
    k::SVector{3,Float64},
    p::SVector{3,Float64},
    q::SVector{3,Float64},
    parameters::Parameters,
) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        ξ_k = ξ(k),
        ξ_p = ξ(p),
        ξ_q = ξ(q),
        ξ_kpq = ξ(k + p - q)

        #! format: off
        -((1 / 2) * D + (3) * D_BQ * S^2 + (3 / 2) * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p - ξ_q - ξ_kpq)) -
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(k) * exp(im * (1 / 2) * (ξ_p - ξ_k - ξ_q - ξ_kpq)) -
        (1 / 2) * K * S^2 * γ(k + p) * exp(-im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(-q) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q - ξ_kpq)) +
        ((1 / 2) * J + (1 / 2) * λ + (3) * K * S^2) * γ(k - q) * exp(im * (1 / 2) * (ξ_p + ξ_q - ξ_k - ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(k + p - q) * exp(im * (1 / 2) * (ξ_q - ξ_k - ξ_p - ξ_kpq)) -
        (1 / 2) * K * S^2 * γ(-k - p) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) -
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(-p) * exp(im * (1 / 2) * (ξ_p + ξ_q + ξ_kpq - ξ_k)) -
        ((1 / 2) * D + (3) * D_BQ * S^2 + (3 / 2) * K * S^2) * exp(im * (1 / 2) * (ξ_q + ξ_kpq - ξ_k - ξ_p))
        #! format: on
    end

V_2212(
    k::SVector{3,Float64},
    p::SVector{3,Float64},
    q::SVector{3,Float64},
    parameters::Parameters,
) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        ξ_k = ξ(k),
        ξ_p = ξ(p),
        ξ_q = ξ(q),
        ξ_kpq = ξ(k + p - q)

        #! format: off
        -(D + 6 * D_BQ * S^2 + (3) * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p - ξ_q - ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * exp(-im * (1 / 2) * (ξ_q + ξ_kpq)) * (γ(k) * exp(im * (1 / 2) * (ξ_p - ξ_k)) - γ(p) * exp(im * (1 / 2) * (ξ_k - ξ_p))) +
        K * S^2 * γ(k + p) * exp(-im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 4) * J + (5 / 2) * K * S^2) * γ(-q) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q - ξ_kpq)) +
        ((1 / 2) * J + (1 / 2) * λ + (3) * K * S^2) * exp(im * (1 / 2) * (ξ_q - ξ_kpq)) * ( γ(p - q) * exp(im * (1 / 2) * (ξ_k - ξ_p)) - γ(k - q) * exp(im * (1 / 2) * (ξ_p - ξ_k)) ) -
        ((1 / 4) * J + (5 / 2) * K * S^2) * γ(k + p - q) * exp(im * (1 / 2) * (ξ_q - ξ_k - ξ_p - ξ_kpq)) -
        K * S^2 * γ(-k - p) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * exp(im * (1 / 2) * (ξ_q + ξ_kpq)) * (γ(-p) * exp(im * (1 / 2) * (ξ_p - ξ_k)) - γ(-k) * exp(im * (1 / 2) * (ξ_k - ξ_p))) +
        (D + 6 * D_BQ * S^2 + (3) * K * S^2) * exp(im * (1 / 2) * (ξ_q + ξ_kpq - ξ_k - ξ_p))
        #! format: on
    end

V_2222(
    k::SVector{3,Float64},
    p::SVector{3,Float64},
    q::SVector{3,Float64},
    parameters::Parameters,
) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        ξ_k = ξ(k),
        ξ_p = ξ(p),
        ξ_q = ξ(q),
        ξ_kpq = ξ(k + p - q)

        #! format: off
        -((1 / 2) * D + (3) * D_BQ * S^2 + (3 / 2) * K * S^2) * exp(im * (1 / 2) * (ξ_k + ξ_p - ξ_q - ξ_kpq)) -
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(k) * exp(im * (1 / 2) * (ξ_p - ξ_k - ξ_q - ξ_kpq)) -
        (1 / 2) * K * S^2 * γ(k + p) * exp(-im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(-q) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q - ξ_kpq)) -
        ((1 / 2) * J + (1 / 2) * λ + (3) * K * S^2) * γ(k - q) * exp(im * (1 / 2) * (ξ_p + ξ_q - ξ_k - ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(k + p - q) * exp(im * (1 / 2) * (ξ_q - ξ_k - ξ_p - ξ_kpq)) -
        (1 / 2) * K * S^2 * γ(-k - p) * exp(im * (1 / 2) * (ξ_k + ξ_p + ξ_q + ξ_kpq)) +
        ((1 / 8) * J + (5 / 4) * K * S^2) * γ(-p) * exp(im * (1 / 2) * (ξ_p + ξ_q + ξ_kpq - ξ_k)) -
        ((1 / 2) * D + (3) * D_BQ * S^2 + (3 / 2) * K * S^2) * exp(im * (1 / 2) * (ξ_q + ξ_kpq - ξ_k - ξ_p))
        #! format: on
    end
