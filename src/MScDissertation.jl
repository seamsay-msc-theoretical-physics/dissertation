"TODO: All the hand-done analytical stuff should be in Julia using Symbolics. We can get around the lack of (implemented) non-commutative algebra by defining variables for each term (though this will mean that we can't do the very first few transformations computationally)."
module MScDissertation

using Base: parameter_upper_bound
include("code/biquadratic.jl")
include("code/Mook.jl")
include("code/Plots.jl")

end  # module
