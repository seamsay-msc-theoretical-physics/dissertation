function mismatched()
    open = ('(', '[', '{')
    close = (')', ']', '}')
    file = read("src/Vertices.tex", String)
    stack = Tuple{Int,Int,Int,Char}[]
    index = 1
    line = 1
    column = 1
    while index < lastindex(file)
        character = file[index]
        current = (index, line, column, character)

        if character in open
            push!(stack, current)
        end

        if character == '\n'
            line += 1
            column = 1
        else
            column += 1
        end

        if (close_type = findfirst(==(character), close)) !== nothing
            if isempty(stack)
                error("Unmatched $character at $index ($line:$column).")
            end

            open_index, open_line, open_column, open_character = pop!(stack)
            open_type = findfirst(==(open_character), open)
            @assert !isnothing(open_type)

            if close_type != open_type
                error(
                    "Mismatch $open_character with $character at $open_index ($open_line:$open_column) and $index ($line:$column).",
                )
            end
        end

        index = nextind(file, index)
    end
end
