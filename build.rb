require 'asciidoctor'
require 'asciidoctor-latex'
require 'asciidoctor-latex-bibtex'
require 'fileutils'
require 'tmpdir'

ASSET_DIR = 'assets'
INKSCAPE_FILES = ['four-operator-diagrams.svg', 'momentum-space-path.svg']

Dir.mktmpdir do |target|
    Asciidoctor.convert_file('src/Main.adoc', backend: 'latex', to_dir: target, safe: :unsafe)

    FileUtils.copy_entry(ASSET_DIR, File.join(target, ASSET_DIR))
    INKSCAPE_FILES.each do |file|
        output = File.basename(file, '.svg') + '.pdf'
        input = File.basename(file, '.svg') + '.pdf_tex'

        system(
            'inkscape',
            '-D',
            '--export-latex',
            File.join(target, ASSET_DIR, file),
            '-o', File.join(target, output),
            exception: true,
        )

        # NOTE: This is so that the input file can refer to the file as `assets/<file>.pdf_tex`.
        FileUtils.copy(File.join(target, input), File.join(target, ASSET_DIR, input))
    end
    system(
        'julia',
        '--project=.',
        '-e', 'using MScDissertation; MScDissertation.Plots.generate(ARGS[1])',
        File.join(target, ASSET_DIR),
        exception: true,
    )

    if !(ARGV & ['-b', '--wait-before-latex']).empty?
        puts target
        STDIN.gets
    end

    system('rubber', '--inplace', '-m', 'xelatex', File.join(target, 'Main.tex'), exception: true)

    if !(ARGV & ['-a', '--wait-after-latex']).empty?
        puts target
        STDIN.gets
    end

    FileUtils.copy(File.join(target, 'Main.pdf'), 'Main.pdf')
end
